---
pagetitle: Marko Tkalčič - Home
---
 
 
# Marko Tkalčič 
 
 
<span class="menu-button" style="font-weight: 700">[Home](index.html) </span> 
<span  class="menu-button" >[About](about.html) </span> 
<span  class="menu-button" >[Research](research.html) </span> 
<span  class="menu-button" >[Publications](publications.html)  </span> 
<span  class="menu-button" >[Teaching](teaching.html)  </span> 
<span  class="menu-button" >[CV](cv.html) </span> 
<span  class="menu-button" >[Resources](resources.html) </span> 
<span  class="menu-button" >[Contact](contact.html)</span> 
 
 

#
 
## Home
 
<img src="resources/markot3.jpg" id="profile_pic" style="float:left;width:20%;"/> 

Full Professor <br>
[University of Primorska Faculty of Mathematics, Natural Sciences and Information Technologies (UP FAMNIT)](https://www.famnit.upr.si/en/) <br>
Glagoljaška 8, SI-6000 Koper, Slovenia   <br>
E-mail: marko.tkalcic at gmail.com  <br>
Latex escape sequence: <pre>Marko Tkal\v{c}i\v{c}</pre><br>


#  
 
 
I am full professor at the Faculty of Mathematics, Natural Sciences and Information Technologies (FAMNIT) at the University of Primorska in Koper, Slovenia. I aim at improving personalized services (e.g. recommender systems) through the usage of psychological models in personalization algorithms. To achieve this, I use diverse research methodologies, including data mining, machine learning, and user studies.
 
If you are further interested in my work, please download my [CV](resources/Tkalcic_CV_Public.pdf).
 
### Updates

2024-11-20
: I have been promoted to full professor.

2024-10-01
: I will be a keynote speaker at the [HCAI SYMPOSIUM](https://hcai-ovgu.github.io/symposium2024/speakers.html), which will be held at the Faculty of Computer Science, Otto von Guericke University Magdeburg, in November 4-5, 2024. 

2024-05-13
: Call for papers for the special issue on **Recommender Systems for Good** in the [ACM Transactions on Recommender Systems](https://dl.acm.org/pb-assets/static_journal_pages/tors/pdf/TORS_SI-Recommender-Systems-for-Good-1715281856967.pdf) coediting with [Nava Tintarev](http://navatintarev.com/), [Noemi Mauro](https://noemi-mauro.github.io/), [Antonela Tommasel](https://github.com/tommantonela), and [Alan Said](https://alansaid.com/)

2024-05-08
: The edited book Ferwerda, B., Graus, M., Germanakos, P., & Tkalčič, M. (Eds.). (2024). [A Human-Centered Perspective of Intelligent Personalized Environments and Systems](https://link.springer.com/book/10.1007/978-3-031-55109-3). Springer Nature Switzerland. https://doi.org/10.1007/978-3-031-55109-3 is finally out!

2024-04-29
: (Position is filled) **I am hiring!** TLDR: PhD position, full-time employment (40 hrs/week), duration four years (start October 2024), research and teaching. More info at [PhD Call](phd_2024.html)

2024-04-09
: Proud to announce that my PhD student Elham Motamedi succesfully defended her thesis. 

2024-03-08
: Keynote at the [PsyIAS](https://sites.google.com/view/psyias/keynote?authuser=0) workshop in conjunction with WSDM 2024

2024-02-19
: **I am hiring!** TLDR: postdoc position (PhD required), full-time employment (40 hrs/week), duration 2 years (preferred start October 2024), research and teaching. More info at [Postdoc Call](postdoc_2024.html)

2023-12-15
: My PhD student Elham Motamedi published the paper [Predicting movies’ eudaimonic and hedonic scores: A machine learning approach using metadata, audio and visual features](https://doi.org/10.1016/j.ipm.2023.103610) in the prestigious journal [Elsevier Information Processing & Management](https://www.sciencedirect.com/journal/information-processing-and-management) (Impact Factor 2022 8.6)

2023-11-06
: Keynote at the CREAI Workshop on AI and  Creativity in conjunction with AIxIA 2023 in Rome [https://creai.github.io/creai2023/program.html](https://creai.github.io/creai2023/program.html)

2023-09-15
: Keynote at the IntRS workshop at RecSys 2023 in Singapore: [https://intrs2023.wordpress.com/program-and-proceedings/](https://intrs2023.wordpress.com/program-and-proceedings/)

2023-04-02
: I will have two presentations in Bergen (Norway): a lecture for students at the University of Bergen on 13.4.2023 and another one at the [MediaFutures institute](https://mediafutures.no/event/mediafutures-seminar-computational-psychology-in-recommender-systems-with-marko-tkalcic-university-of-primorska/).

2022-06-20
: I will give a keynote at the [12th Italian Information Retrieval Workshop](https://recsyspolimi.github.io/iir2022/talks/talk-tkalcic/) on June 30th, 2022 in Milano, Italy


<!--
2022-04-02
: Co-editor of the special issue on [Future Generation Personality Prediction Using Social Media Data and Physiology Signals](https://www.hindawi.com/journals/cin/si/518745/) in Hindawi Computational Intelligence and Neuroscience (impact factor = 3.6). Submission deadline is 15. July 2022
-->

2022-03-15
: Organizing the [HAAPIE](http://haapie.cs.ucy.ac.cy/) workshop at ACM UMAP 2022. Deadline for papers: 15. April 2022

2021-12-10
: I'll be chairing the Intelligent User Interfaces track with [Elisabeth Lex](https://elisabethlex.info/) at the [2022 ACM UMAP](https://www.um.org/umap2022/call-for-papers/?fbclid=IwAR3DsMqEj48q25qPTmQeY322AcbBA6s9agOd_IX4yUBBcTj2k-bKZFhrcdA) conference in Barcelona. Abstract submission deadline: 10. February 2022

2021-11-15
: Chairing the Demos and Posters at the [International Conference on Web Engineering (ICWE)](https://icwe2022.webengineering.org/organizing-committee/) conference with [Yashar Deldjoo](https://yasdel.github.io/) and [Irene Garrigos](https://cvnet.cpd.ua.es/curriculum-breve/es/garrigos-fernandez-irene/12276) in beautiful Bari. Submission deadline: 19. February 2022

2021-11-08
: Organized the [ACM HCI Slovenia](https://hci.si/hci-si-2021/) conference in Koper, the first live event I attended in a while. Great to have had [Peter Knees](https://www.ifs.tuwien.ac.at/~knees/) and [Gregor Geršak](http://lmk.fe.uni-lj.si/o-nas/zaposleni/gregor-gersak/) as keynote speakers

2021-11-05
: Happy to share that I made it into the Stanford list of the top-scientists cited worldwide [https://elsevier.digitalcommonsdata.com/datasets/btchxktzyw/3](https://elsevier.digitalcommonsdata.com/datasets/btchxktzyw/3)

2021-06-17
: Welcome to [ACM UMAP 2021](https://www.um.org/umap2021/program/schedule)

2021-06-17
: The paper [Investigating the impact of recommender systems on user-based and item-based popularity bias](https://doi.org/10.1016/j.ipm.2021.102655) coauthored with Mehdi Elahi, Danial Khosh Kholg, Mohammad Sina Kiarostami, Sorush Saghar, and Shiva Parsa Rad has been published in [Elsevier Information Processing and Management](https://doi.org/10.1016/j.ipm.2021.102655)

2021-05-05
: Co-editing the focus section on [User Modeling and Recommendations](https://www.frontiersin.org/research-topics/19653/user-modeling-and-recommendations) in  Frontiers in Big Data Mining and Management with [Denis Helić](https://courses.isds.tugraz.at/dhelic/) and [Ujwal Gadiraju](http://ujwalgadiraju.com/). Deadline: 20. September 2021

2021-05-15
: Co-editing the Special issue on [Group Recommender Systems](https://ludovicoboratto.github.io/umuaigrouprecsys/) in Springer User Modeling and User-adapted Interaction with [Ludovico Boratto](https://www.ludovicoboratto.com/), [Alexander Felfernig](http://www.felfernig.eu/), and [Martin Stettinger](http://ase.ist.tugraz.at/). Deadline: 15. June 2022

2020-11-11
:	The paper [A Comparison of Human and Computational Melody Prediction Through Familiarity and Expertise](https://www.frontiersin.org/articles/10.3389/fpsyg.2020.557398/abstract) co-authored with [Matevž Pesek](http://matevzpesek.si/), [Anja Podlesek](http://psy.ff.uni-lj.si/slo/ljudje/Anja.Podlesek), Špela Medvešek, and [Matija Marolt](https://fri.uni-lj.si/en/employees/matija-marolt)  has been accepted to the research topic [The Effects of Music on Cognition and Action](https://www.frontiersin.org/research-topics/10709/the-effects-of-music-on-cognition-and-action) in [Frontiers in Psychology/Auditory Cognitive Neuroscience](https://www.frontiersin.org/journals/psychology/sections/auditory-cognitive-neuroscience#)

2020-10-20
:	Happy to share that I was among the best reviewers at [ISMIR - 21st International Society for Music Information Retrieval Conference](https://www.ismir2020.net/awards/)

2020-09-23
:	I am happy to announce that I will be, with [Nava Tintarev](http://navatintarev.com/), Program Co-chair of the [29th Conference on User Modeling, Adaptation and Personalization (UMAP) 2021](https://www.um.org/umap2021/), which will be held in Utrecht (the Netherlands) in June 2021

2020-09-22
:	I will give a keynote at the [HCI-IS](https://hci.si/2020/10/03/hci-is-2020-schedule/), hosted by the Slovenian ACM SIGCHI Chapter 

2020-08-25
:	I will give a keynote at [ISMIS 2020 - 25th International Symposium on Methodologies for Intelligent Systems](https://ismis.ist.tugraz.at/) with the title *Complementing Behavioural Modeling with Cognitive Modeling for Better Recommendations*

2020-06-25 
:	I will chair the RecSys 3 session at the [ACM UMAP 2020 conference](https://um.org/umap2020/attending/program/) on 16. July 2020

2020-04-15
:	I will chair the User Modeling-A(3) session at [The Web Conference 2020](https://www2020.thewebconf.org/). Schedule and other info here: [https://www2020.citi.sinica.edu.tw/schedule/research_track/#User-Modeling-A-3](https://www2020.citi.sinica.edu.tw/schedule/research_track/#User-Modeling-A-3)

2020-01-14
:	I was just awarded the Italian habilitation of Full Professor both for the sectors [ASN 01/B1 - INFORMATICA](https://asn18.cineca.it/pubblico/miur/esito-abilitato/01%252FB1/1/3) and [09/H1 Sistemi di elaborazione delle informazioni](https://asn18.cineca.it/pubblico/miur/esito-abilitato/09%252FH1/1/3)

2019-12-23
:	Interview for the Russian business portal RBK on cognitive and behavioural user modeling [https://youtu.be/p-HVFER9Kd0](https://youtu.be/p-HVFER9Kd0)

2019-11-15
:	The video of my keynote talk at the AI Journey conference in Moscow is available at [this YouTube link](https://youtu.be/9_cVEc6gX2k?t=1663).

2019-10-23
: Together with [Li Chen](http://www.comp.hkbu.edu.hk/~lichen/) and [Bruce Ferwerda](http://www.bruceferwerda.com/?i=1) I am editing the focus section [Psychological Models for Personalized Human-Computer Interaction (HCI)](https://www.frontiersin.org/research-topics/11465/psychological-models-for-personalized-human-computer-interaction-hci) in [Frontiers in Psychology](https://www.frontiersin.org/journals/psychology#) (Impact Factor 2.129) and  [Frontiers in Computer Science](https://www.frontiersin.org/journals/computer-science#). Submission **deadline**: 03 March 2020

2019-10-22
:	I will give an invited talk at the [AI Journey](https://ai-journey.ru/en/conference-moscow/science-day-program) conference in Moscow on November 9th 2019. The title of the talk will be *From Behavioural to Cognitive Modeling in Recommender Systems*

2019-10-01
:	I started my tenured position as associate professor at the [University of Primorska Faculty of Mathematics, Natural Sciences and Information Technologies (UP FAMNIT)](https://www.famnit.upr.si/en/)

2019-08-15
: Together with [Mehdi Elahi](https://www.unibz.it/en/faculties/computer-science/academic-staff/person/31096-mehdi-elahi) I am editing the special issue [Algorithms for Personalization Techniques and Recommender Systems](https://www.mdpi.com/journal/algorithms/special_issues/Algorithms_Recommender_Systems) in the [MDPI Algorithms](https://www.mdpi.com/journal/algorithms) journal. Submission **deadline**: 20. November 2019

2019-07-01
:	The video of my presentation at ACM IUI 2019 is available here: [Video: Prediction of music pairwise preferences from facial expressions](https://www.youtube.com/watch?v=sUGag1hqpMY)   Paper: [Paper: Prediction of music pairwise preferences from facial expressions](https://dl.acm.org/citation.cfm?id=3302266)

2019-04-25
:	Together with [Sole Pera](https://solepera.github.io/) I am [chairing](https://recsys.acm.org/recsys19/committees/) the [Late-breaking Results - Posters](https://recsys.acm.org/recsys19/call/#content-tab-1-5-tab) and [Demos](https://recsys.acm.org/recsys19/call/#content-tab-1-4-tab) at the [ACM RecSys 2019](https://recsys.acm.org/recsys19/) conference. The submission deadline is 1. July 2019

2019-03-04
:	I am co-organizing the [3rd Workshop on Theory-Informed User Modeling for Tailoring and Personalizing Interfaces (HUMANIZE)](http://www.humanize-workshop.org/) at [ACM IUI 2019](https://iui.acm.org/2019/) conference. With [Mark Graus](http://www.markgraus.net/),  [Bruce Ferwerda](http://www.bruceferwerda.com/?i=1), and [Panagiotis Germanakos](http://scrat.cs.ucy.ac.cy/pgerman/)

2019-02-27
:	Our paper [Personality and taxonomy preferences, and the influence of category choice on the user experience for music streaming services](https://link.springer.com/article/10.1007/s11042-019-7336-7) has been published in the journal [Multimedia Tools and Applications](https://link.springer.com/journal/11042). With [Bruce Ferwerda](http://www.bruceferwerda.com/?i=1), Emily Yang, and [Markus Schedl](http://www.cp.jku.at/people/schedl/)

2018-12-08
:	Our paper [Prediction of music pairwise preferences from facial expressions](https://dl.acm.org/citation.cfm?id=3302266) has been accepted as a long paper at the [ACM IUI 2019](https://iui.acm.org/2019/program.html) conference. With Nima Maleki, [Matevz Pesek](https://fri.uni-lj.si/en/employees/matevz-pesek), [Mehdi Elahi](https://www.unibz.it/en/faculties/computer-science/academic-staff/person/31096-mehdi-elahi), [Francesco Ricci](http://www.inf.unibz.it/~ricci/), and [Matija Marolt](https://www.fri.uni-lj.si/en/employees/matija-marolt)

2018-07-12
:  I will give a tutorial on [Emotions and Personality in Recommender Systems](https://recsys.acm.org/recsys18/tutorials/#content-tab-1-2-tab) on October 2 at [RecSys 2018](https://recsys.acm.org/recsys18/) in Vancouver. The material can be found [here](https://gitlab.com/markotka/recsys2018-tutorial)
 
2018-06-01
:  I am co-organizing the [International Workshop on Decision Making and Recommender Systems 2018](http://dmrsworkshop.inf.unibz.it/) (29-30 November 2018, Bozen-Bolzano, Italy) with [Francesco Ricci](http://www.inf.unibz.it/~ricci/), [Markus Zanker](https://www.unibz.it/en/faculties/computer-science/academic-staff/person/3466-markus-zanker) and [Mehdi Elahi](https://www.unibz.it/en/faculties/computer-science/academic-staff/person/31096-mehdi-elahi).
 
2018-03-15
:  I am co-editing the UMUAI [Special Issue on User Modeling for Personalized Interaction with Music](http://www.cp.jku.at/journals/umuai_si_music.html) with [Markus Schedl](http://www.cp.jku.at/people/schedl/) and [Peter Knees](https://www.ifs.tuwien.ac.at/%7Eknees/). Submission deadline for extended abstracts: **1. June 2018**.
 
2018-01-11
:  Our book [Group Recommender Systems : An Introduction (Springer)](https://www.springer.com/gp/book/9783319750668), edited together with [Alexander Felfernig](http://www.ist.tugraz.at/felfernig/), [Martin Stettinger](https://scholar.google.at/citations?user=uOlG-BoAAAAJ&hl=de) and [Ludovico Boratto](http://ludovicoboratto.com/), will be on the shelves from March 2018.
 
2017-12-06
:  I have been habilitated as **Associate Professor** (Professore di II. fascia) in Italy by the national habilitation committee (Abilitazione Scientifica Nazionale) for the area [09/H1](https://asn16.cineca.it/pubblico/miur/esito/09%252FH1/2/3) (ING-INF/05).
 
2017-10-18
:   Starting with January 2018 I will be member of the editorial board of the Springer [User Modeling and User-Adapted 
Interaction journal](http://www.springer.com/computer/hci/journal/11257?detailsPage=editorialBoard) (2016 impact factor 3.625).
 
 
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
 
  ga('create', 'UA-102616018-1', 'auto');
  ga('send', 'pageview');
</script>
 
 

