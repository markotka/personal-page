---
pagetitle: Marko Tkalcic - About
---

# Marko Tkalčič  

<span class="menu-button">[Home](index.html) </span> 
<span  class="menu-button"  style="font-weight: 700">[About](about.html) </span> 
<span  class="menu-button" >[Research](research.html) </span> 
<span  class="menu-button" >[Publications](publications.html)  </span> 
<span  class="menu-button" >[Teaching](teaching.html)  </span> 
<span  class="menu-button" >[CV](cv.html) </span> 
<span  class="menu-button" >[Resources](resources.html) </span> 
<span  class="menu-button" >[Contact](contact.html)</span> 

#

## About

I am full professor at the Faculty of Mathematics, Natural Sciences and Information Technologies (FAMNIT) at the University of Primorska in Koper, Slovenia. My research focuses on psychological aspects of personalization in connection with computational models. Specific interests include the usage of psychological models (e.g. personality, emotions etc.) for improving personalized applications (e.g. recommender systems) and the usage of social media to infer the psychological models. I use diverse research methodologies, including data mining, machine learning, and user studies.

<img src="resources/aij_marko_1.jpg"  style="float:left;width:40%;"/> 

I currently work at the Faculty of Mathematics, Natural Sciences and Information Technologies (FAMNIT) at the University of Primorska in Koper, Slovenia, where I teach and conduct research.

Before coming to Koper, I was assistant professor at the Free University of Bozen-Bolzano in the group led by Francesco Ricci. Before coming to Bolzano I was a post-doctoral researcher at the Department of Computational Perception at the Johannes Kepler University in Linz, Austria. In the group led by Gerhard Widmer, I worked with Markus Schedl on personality-based music information retrieval system. Prior to that I was a PhD student and post-doctoral researcher at the Faculty of Electrical Engineering at the University of Ljubljana in Slovenia, where I worked on the usage of emotions and personality in image recommender systems. 

I have collaborated and co-authored publications with numerous researchers around the world, including
Francesco Ricci (Free University of Bolzano), 
Domonkos Tikk (Gravity), 
Emilia Gomez (UPF), 
Giovanni Semeraro (Univ. Bari),
Li Chen (Hong Kong Batist University), 
Daniele Quercia (Bell Labs), 
Sabine Graf (Athabasca University), 
Franca Garzotto and Paolo Cremonesi (Politecnico di Milano), and Martha Larsson (Radboud University).

I have published in prestigious journals, such as *UMUAI, IEEE Transactions on Affective Computing, Elsevier Information Sciences,* and *IEEE Transactions on Multimedia*. My work has been presented at many conferences, including *WWW, CHI, IUI, RecSys, UMAP, SAC, ISMIR, ECIR, ICMR* and others. I am a member of the editorial board of the *UMUAI* journal. I am an editor of the books [*Group Recommender Systems, An Introduction *](https://www.springer.com/de/book/9783319750668) and [*Emotions and Personality in Personalized Services*](https://link.springer.com/book/10.1007/978-3-319-31413-6). I have also edited special issues in the *UMUAI* and *IxD&A* journals. Furthermore, I was  co-organizer of ACM UMAP 2017, 2018 and 2019, and ACM RecSys 2017, 2018 and 2019 and numerous workshops. I serve as PC member at prestigious conferences (WWW, ACM MM, ACM RecSys, ACM IUI, ACM UMAP) and often review for high impact-factor journals (UMUAI, Elsevier Information Sciences, IEEE Transactions on Multimedia, ACM TIST).

If you are further interested in my work, please download my [CV](resources/Tkalcic_CV_Public.pdf).


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-102616018-1', 'auto');
  ga('send', 'pageview');
</script>