


---
pagetitle: Marko Tkalcic - Teaching
---



# Marko Tkalčič 

<span class="menu-button">[Home](index.html) </span> 
<span  class="menu-button" >[About](about.html) </span> 
<span  class="menu-button"  >[Research](research.html) </span> 
<span  class="menu-button" >[Publications](publications.html)  </span> 
<span  class="menu-button" style="font-weight: 700">[Teaching](teaching.html)  </span> 
<span  class="menu-button" >[CV](cv.html) </span>
<span  class="menu-button" >[Resources](resources.html) </span>  
<span  class="menu-button" >[Contact](contact.html)</span> 

#

## Teaching

### Programming Project (Spring 2019)

Course
: Programming Project

University
:	Free University of BOzen-Bolzano

Course web page
:	[https://ole.unibz.it/course/view.php?id=4743](https://ole.unibz.it/course/view.php?id=4743)

<hr>

### Programming for Data Analytics (Fall 2018)

Course
: Programming for Data Analytics

University
:	Free University of BOzen-Bolzano

Course web page
:	[https://ole.unibz.it/course/view.php?id=4404](https://ole.unibz.it/course/view.php?id=4404)

<hr>

### Decision Support Systems (Fall 2018)

Course:
:	602.315 (17W) Decision Support Systems 

University
:	Alpen-Adria-Universität Klagenfurt, Austria

Course web page 
:	[https://campus.aau.at/studium/course/95864](https://campus.aau.at/studium/course/95864)

<hr>

### Programming Project (Spring 2018)

Course
:	Programming Project

University
:	Free University of Bozen-Bolzano, Italy

Course web page
:	[http://www.inf.unibz.it/~tkalcic/teaching_pp2017-2018.html](http://www.inf.unibz.it/~tkalcic/teaching_pp2017-2018.html)

<hr>

### Decision Support Systems (Fall 2017)

Course:
:	602.315 (17W) Decision Support Systems 

University
:	Alpen-Adria-Universität Klagenfurt, Austria

Course web page 
:	[https://campus.aau.at/studium/course/91810?lang=en](https://campus.aau.at/studium/course/91810?lang=en)


**Lecture Notes and Other Material** 

1
: 	[Introduction](resources/Tkalcic-DSS-201718-01-Introduction.slides.pdf)

2
: 	[Modeling Decisions 1](resources/Tkalcic-DSS-201718-02_ModelingDecisions-1.slides.pdf)

2a
: 	[tree-pennzoil-texaco.xlsx](resources/tree-pennzoil-texaco.xlsx)

3
: 	[Modeling Decisions 2](resources/Tkalcic-DSS-201718-03_ModelingDecisions-2.slides.pdf)
 
3a
: 	[tornado.xls](resources/tornado.xlsx)

4
:	[Modeling Uncertainty 1](resources/Tkalcic-DSS-201718-04_ModelingUncertainty-1.slides.pdf)

5
:	[Modeling Uncertainty 2](resources/Tkalcic-DSS-201718-05_ModelingUncertainty-2.slides.pdf)

5a
: 	[goals.xls](resources/goals.xlsx)

5b
: 	[MonteCarlo.xls](resources/MonteCarlo.xlsx)

5c
:	[R code](resources/r_fitting.zip)

6
:	[Modeling Preferences](resources/Tkalcic-DSS-201718-06_ModelingPreferences-1.slides.pdf)

7
:	[Psychological Aspects](resources/Tkalcic-DSS-201718-07_PsychologicalAspects.slides.pdf)

8
:	[Clustering](resources/Tkalcic-DSS-201718-08-DataMining_Clustering.slides.pdf)

8a
:	[iris.arff](resources/iris.arff)

8b
:	[mall.csv](resources/mall.csv)


**Instructions for Project**

Project instructions:

- each student has to submit an individual project (no group projects)
- identify a reasonable decision problem with 
	- 5+ unknown variables
	- 2+ objectives
- model the distributions
	- at least one variable with historical data (find on-line historical data for the unknown variables)
- apply decision analysis tools as needed
	- build the influence diagram
	- build the decision tree
	- calculate the EMV of all choice paths
	- build the risk profile
	- perform one-way sensitivity analysis
	- build a utility function
	- calculate the EV 
- apply simulation
	- perform a Monte Carlo simulation
- write a report with the following structure
	- introduction
	- decision problem description
	- modeling of unknown variables (include links to online sources)
	- decision analysis
	- simulation
	- conclusion


Submission Instructions

- send report (PDF file) via email to marko.tkalcic@aau.at
- file naming convention: "DSS-Project-studentID.pdf" (e.g. DSS-Project-123456789.pdf)
- email subject line: "DSS-Project-studentID" (e.g. DSS-Project-123456789)
- deadline: 4. December 2017, 23:59 CET


**Project Results**

The results of the project can be found here: [results](resources/00_DSS_2017_ProjectResults.pdf)


**Grading**  

- There will be a written exam
- Exam-like quick quizzes throughout the course
- Optional: Project (50% of the total exam score)
- Grading: 
	- score = project assessment score [0-50] + exam score [0-100]
	- if score>100 then score=100
	- pass score (1,2,3,4) >= 50

**Exam Results**

Results of the exam from 22.12.2017 are available [here](resources/20171222_DSS_ExamResults.pdf)

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-102616018-1', 'auto');
  ga('send', 'pageview');
</script>