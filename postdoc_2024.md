---
pagetitle: Marko Tkalčič - Postdoc Position
---
 
 
# Marko Tkalčič 
 
 
<span class="menu-button" style="font-weight: 700">[Home](index.html) </span> 
<span  class="menu-button" >[About](about.html) </span> 
<span  class="menu-button" >[Research](research.html) </span> 
<span  class="menu-button" >[Publications](publications.html)  </span> 
<span  class="menu-button" >[Teaching](teaching.html)  </span> 
<span  class="menu-button" >[CV](cv.html) </span> 
<span  class="menu-button" >[Resources](resources.html) </span> 
<span  class="menu-button" >[Contact](contact.html)</span> 
 
 

#
 
## Open Postdoc Position

Koper, 19. Feb 2024

### TLDR

Postdoc position (PhD required), full-time employment (40 hrs/week), duration 2 years (preferred start October 2024), research and teaching, supervisor: Marko Tkalčič, University of Primorska, Koper, Slovenia.

### Full Description

**Research topics:**

- psychologically-informed user modeling,
- psychologically-informed item modeling,
- inference of user and item characteristics,
- explanations,
- recommender systems.

**Details:** 

The Department of Information Sciences and Technologies (DIST) at the Faculty of Mathematics, Natural Sciences and Information
Technologies (FAMNIT) of the University of Primorska is seeking a top early-career researcher for a postdoctoral position in the area of psychologically-informed user modeling under the supervision of Assoc. Prof. Dr. Marko Tkalčič.

The project will explore how psychologically-informed features can be used to model users, items, infer user characteristics from digital traces, infer item characteristics, provide explanations and recommendations in the music and film domains. The methodologies will include user studies and machine learning. The candidate will work in a great team with Marko Tkalčič as part of the [HICUP lab](https://hicup.famnit.upr.si/) in the beautiful Mediterranean city of Koper (the beach is only a 3 min walk from the office). 

- Full-time employment (40 hrs/week). Full social security.
- The position is for two years.
- The preferred starting date is October 2024.
- The candidate must have (or expect to obtain shortly) a PhD in computer science or in an area relevant to the research topics.
- The ideal candidate should posses expertise in the following areas:
  - recommender systems,
  - machine learning,
  - user modeling.
- Expertise in one or more of the following areas will be appreciated:
  - ML explainability,  
  - computational psychology,
  - computational social science,
  - media-related knowledge (musicology etc.).
- The position includes a small teaching load (1 course/semester, 3-4 hrs/week).
- The position comes with funding for travel (conferences, visitors).

**Application Process:**

The applications will be assessed on a rolling basis until the position is filled. To apply, send an email to [marko.tkalcic@famnit.upr.si](mailto:marko.tkalcic@famnit.upr.si) with the following documentation:

- motivation letter,
- CV,
- list of publications,
- research statement,
- names and email addresses of two to three references.




<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
 
  ga('create', 'UA-102616018-1', 'auto');
  ga('send', 'pageview');
</script>
 
 

