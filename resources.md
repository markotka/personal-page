---
pagetitle: Marko Tkalcic - Resources
---

# Marko Tkalčič 

<span class="menu-button">[Home](index.html) </span> 
<span  class="menu-button" >[About](about.html) </span> 
<span  class="menu-button" >[Research](research.html) </span> 
<span  class="menu-button" >[Publications](publications.html)  </span> 
<span  class="menu-button" >[Teaching](teaching.html)  </span> 
<span  class="menu-button" >[CV](cv.html) </span> 
<span  class="menu-button" style="font-weight: 700">[Resources](resources.html) </span> 
<span  class="menu-button" >[Contact](contact.html)</span> 


# 

## Resources


- **LDOS-PerAff-1**:	Please contact me via e-mail for getting access to the LDOS-PerAff-1 dataset. More details about the dataset can be found in the journal paper [The LDOS-PerAff-1 corpus of facial-expression video clips with affective, personality and user-interaction metadata](https://link.springer.com/article/10.1007/s12193-012-0107-7)











If you are further interested in my work, please download my [CV](resources/Tkalcic_CV_Public.pdf).



<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-102616018-1', 'auto');
  ga('send', 'pageview');
</script>