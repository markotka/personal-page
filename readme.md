# README 

Personal webpage on the gitlab website.

## Related resources

### Gitlab static hosting 

The instructions for hosting static websites are available at https://about.gitlab.com/2016/04/07/gitlab-pages-setup/

### Generating HTML from MD

The static pages are created using TSPW [https://github.com/eakbas/TSPW](https://github.com/eakbas/TSPW) and Pandoc. TSPW is basically just a makefile.

## Files and folders

- ```.gitlab-ci.yml``` defines what happens on the gitlab server at each commit. Basically, it copies the folder ```./build/``` to the public repository
- ```makefile``` defines how the compilation of markdown files to html files is done
- ```build``` this folder contains the HTML code


### Makefile

```

```

## How to update the page

1. update the ```.md``` files
2. compile them (using Cmd+B in SublimeText - not in windows) according to the makefile
	- launch the linux shell
	- cd /mnt/c
	- cd /Users/markot/work/htdocs/markotkalcic.gitlab.com
	- make
3. commit to the gitlab page (using the ```git_push_all.sh``` script or manually)
4. the updated page is accessibe at http://markotka.gitlab.io/personal-page/ 

## On Windows

If you don't have Linux or Mac you should install the Windows Subsystem for Linux (WSL). In the search, look for "Turn Windows features on and off". Then tick on "Windows Subsystem for Linux" and follow through





