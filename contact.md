---
pagetitle: Marko Tkalcic - Contact
---

# Marko Tkalčič 


<span class="menu-button">[Home](index.html) </span> 
<span  class="menu-button" >[About](about.html) </span> 
<span  class="menu-button"  >[Research](research.html) </span> 
<span  class="menu-button" >[Publications](publications.html)  </span> 
<span  class="menu-button" >[Teaching](teaching.html)  </span> 
<span  class="menu-button" >[CV](cv.html) </span> 
<span  class="menu-button" >[Resources](resources.html) </span> 
<span  class="menu-button" style="font-weight: 700">[Contact](contact.html)</span> 

#

## Contact

If you want to get in contact with me, please send me an [e-mail](mailto:marko.tkalcic@gmail.com).

### University Contact

|  University Primorskem
|  Faculty of Mathematics, Natural Sciences and Information Technologies
|  Glagoljaška 8
|  SI-6000 Koper
|  Slovenija



### Social Media

| [Google Scholar](https://scholar.google.com/citations?user=JQ2puysAAAAJ&hl=en)
| [Linkedin](https://www.linkedin.com/in/markotkalcic/)
| [Twitter](https://twitter.com/RecSysMare)
| [Skype](skype:markotkalcic)
| [Slideshare](http://www.slideshare.net/markotka)
| [Mendeley](http://www.mendeley.com/profiles/marko-tkalcic/)

If you are further interested in my work, please download my [CV](resources/Tkalcic_CV_Public.pdf).

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-102616018-1', 'auto');
  ga('send', 'pageview');
</script>