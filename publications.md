


---
pagetitle: Marko Tkalcic - Publications
---



# Marko Tkalčič 

<span class="menu-button">[Home](index.html) </span> 
<span  class="menu-button" >[About](about.html) </span> 
<span  class="menu-button"  >[Research](research.html) </span> 
<span  class="menu-button" style="font-weight: 700">[Publications](publications.html)  </span> 
<span  class="menu-button" >[Teaching](teaching.html)  </span> 
<span  class="menu-button" >[CV](cv.html) </span> 
<span  class="menu-button" >[Resources](resources.html) </span> 
<span  class="menu-button" >[Contact](contact.html)</span> 

#



## Publications

Link to my [Google Scholar Profile](https://scholar.google.com/citations?user=JQ2puysAAAAJ&hl=en). If you are further interested in my work, please download my [CV](resources/Tkalcic_CV_Public.pdf).

### Journal Papers 

J16
:	Motamedi, E., Barile, F., & **Tkalčič, M.** (2022). Prediction of Eudaimonic and Hedonic Orientation of Movie Watchers. Applied Sciences, 12(19), 9500. https://doi.org/10.3390/app12199500


J15
:	Elahi, M., Kholgh, D. K., Kiarostami, M. S., Saghari, S., Rad, S. P., & **Tkalčič, M.** (2021). Investigating the impact of recommender systems on user-based and item-based popularity bias. Information Processing & Management, 58(5), 102655. https://doi.org/10.1016/j.ipm.2021.102655
 
J14
:	Pesek, M., Medvešek, Š., Podlesek, A., **Tkalčič, M.**, & Marolt, M. (2020). A Comparison of Human and Computational Melody Prediction Through Familiarity and Expertise. Frontiers in Psychology, 11(December), 1–18. https://doi.org/10.3389/fpsyg.2020.557398

J13 
:	Ferwerda, B., Yang, E., Schedl, M., and **Tkalčič, M.,** (2019). Personality and taxonomy preferences, and the influence of category choice on the user experience for music streaming services. Multimedia Tools and Applications. https://doi.org/10.1007/s11042-019-7336-7

J12
:	Schedl, M., Gomez, E., Trent, E., **Tkalčič, M.,** Eghbal-Zadeh, H., & Martorell, A. (2017). On the Interrelation between Listener Characteristics and the Perception of Emotions in Classical Orchestra Music. IEEE Transactions on Affective Computing, 1–1. https://doi.org/10.1109/TAFFC.2017.2663421


J11
:	Vodlan, T., **Tkalčič, M.,** & Košir, A. (2015). The impact of hesitation, a social signal, on a user’s quality of experience in multimedia content retrieval. Multimedia Tools and Applications, 74(17), 6871–6896. https://doi.org/10.1007/s11042-014-1933-2


J10
:	**Tkalčič, M.,** Odić, A., Košir, A., & Tasič, J. (2013). Affective labeling in a content-based recommender system for images. IEEE Transactions on Multimedia, 15(2), 391–400. https://doi.org/10.1109/TMM.2012.2229970


J9
:	Odić, A., **Tkalčič, M.,** Tasič, J. F., & Košir, A. (2013). Predicting and Detecting the Relevant Contextual Information in a Movie-Recommender System. Interacting with Computers, 25(1), 74–90. https://doi.org/10.1093/iwc/iws003


J8
:	**Tkalčič, M.,** Košir, A., & Tasič, J. (2013). The LDOS-PerAff-1 corpus of facial-expression video clips with affective, personality and user-interaction metadata. Journal on Multimodal User Interfaces, 7(1–2), 143–155. https://doi.org/10.1007/s12193-012-0107-7


J7
:	Odić, A., **Tkalčič, M.,** Tasič, J. F., & Košir, A. (2013). Impact of the Context Relevancy on Ratings Prediction in a Movie-Recommender System. Automatika ‒ Journal for Control, Measurement, Electronics, Computing and Communications, 54(2), 252–262. https://doi.org/10.7305/automatika.54-2.258

 
J6
:	**Tkalčič, M.,** Odić, A., & Košir, A. (2013). The impact of weak ground truth and facial expressiveness on affect detection accuracy from time-continuous videos of facial expressions. Information Sciences, 249, 13–23. https://doi.org/10.1016/j.ins.2013.06.006


J5
:	**Tkalčič, M.,** Košir, A., Dobravec, Š., & Tasič, J. (2011). Emotional properties of latent factors in an image recommender system. Elektrotehniški Vestnik, 78(4), 177–180. Retrieved from http://ev.fe.uni-lj.si/4-2011/Tkalčič.pdf


J4
:	Košir, A., Odić, A., Kunaver, M., **Tkalčič, M.,** & Tasič, J. F. (2011). Database for contextual personalization. Elektrotehniški Vestnik, 78(5), 270–274. Retrieved from http://ev.fe.uni-lj.si/5-2011/Kosir.pdf

J3
:	**Tkalčič, M.,** Kosir, A., & Tasic, J. (2011). Usage of affective computing in recommender systems. Elektrotehniski Vestnik/Electrotechnical Review, 78(1–2), 12–17. Retrieved from http://ev.fe.uni-lj.si/1-2-2011/Tkalčič.pdf

J2
:	**Tkalčič, M.,** Burnik, U., & Košir, A. (2010). Using affective parameters in a content-based recommender system for images. User Modelling and User-Adapted Interaction, 20(4), 279–311. https://doi.org/10.1007/s11257-010-9079-z

J1
:	Grbec, S., **Tkalčič, M.,** & Diaci, J. (2008). The influence of inertial loading on color gamut properties of a TFT LCD display. Displays, 29(1), 18–24. https://doi.org/10.1016/j.displa.2007.06.008



### Conference Papers

C36
:	Motamedi, E., Tkalcic, M., & Szlávik, Z. (2023). Eudaimonic and Hedonic Qualities as Predictors of Music Videos’ Relevance to Users: A Human-Centric Study. Adjunct Proceedings of the 31st ACM Conference on User Modeling, Adaptation and Personalization, 44–49. https://doi.org/10.1145/3563359.3597415

C35
:	Ferwerda, B., Kiunsi, D. R., & Tkalčič, M. (2022). Too Much of a Good Thing: When In-Car Driver Assistance Notifications Become Too Much. 14th International Conference on Automotive User Interfaces and Interactive Vehicular Applications, 79–82. https://doi.org/10.1145/3544999.3552536


C34
:	Tkalčič, M., Motamedi, E., Barile, F., Puc, E., & Mars Bitenc, U. (2022). Prediction of Hedonic and Eudaimonic Characteristics from User Interactions. Adjunct Proceedings of the 30th ACM Conference on User Modeling, Adaptation and Personalization, 366–370. https://doi.org/10.1145/3511047.3537656



C33
:	Reiter-Haas, M., Parada-Cabaleiro, E., Schedl, M., Motamedi, E., **Tkalcic, M.**, & Lex, E. (2021). Predicting Music Relistening Behavior Using the ACT-R Framework. Fifteenth ACM Conference on Recommender Systems, 702–707. https://doi.org/10.1145/3460231.3478846

C32
:	Najafian, S., Delic, A., **Tkalcic, M.**, & Tintarev, N. (2021). Factors Influencing Privacy Concern for Explanations of Group Recommendation. Proceedings of the 29th ACM Conference on User Modeling, Adaptation and Personalization, 14–23. https://doi.org/10.1145/3450613.3456845

C31
:	Elahi, M., Bakhshandegan Moghaddam, F., Hosseini, R., Rimaz, M. H., El Ioini, N., **Tkalcic, M.**, Trattner, C., & Tillo, T. (2021). Recommending Videos in Cold Start With Automatic Visual Tags. Adjunct Proceedings of the 29th ACM Conference on User Modeling, Adaptation and Personalization, 54–60. https://doi.org/10.1145/3450614.3461687

C30
:	Najafian, S., Draws, T., Barile, F., **Tkalcic, M.**, Yang, J., & Tintarev, N. (2021). Exploring User Concerns about Disclosing Location and Emotion Information in Group Recommendations. Proceedings of the 32st ACM Conference on Hypertext and Social Media, 155–164. https://doi.org/10.1145/3465336.3475104

C29
:	Ferwerda, B., & **Tkalčič, M.** (2020). Exploring the Prediction of Personality Traits from Drug Consumption Profiles. Adjunct Publication of the 28th ACM Conference on User Modeling, Adaptation and Personalization, 2–5. https://doi.org/10.1145/3386392.3397589


C28
:	Barile, F., Ricci, F., **Tkalcic, M.**, Magnini, B., Zanoli, R., Lavelli, A., & Speranza, M. (2019). A News Recommender System for Media Monitoring. Web Intelligence 2019. https://doi.org/10.1145/3350546.3352510

C27
:	Rimaz, M. H., Elahi, M., Moghadam, F., Trattner, C., Hosseini, R., & **Tkalčič, M.** (2019). Exploring the Power of Visual Features for Recommendation of Movies. UMAP 2019. https://doi.org/10.1145/3320435.3320470

C26
:	Ferwerda, B., & **Tkalčič, M.** (2019). Exploring Online Music Listening Behaviors of Musically Sophisticated Users. UMAP 2019, 1–5. https://doi.org/10.1145/3314183.3324974

C25
: **Tkalčič, M.,** Maleki, N., Pesek, M., Elahi, M., Ricci, F., & Marolt, M. (2019). Prediction of music pairwise preferences from facial expressions. In Proceedings of the 24th International Conference on Intelligent User Interfaces - IUI ’19 (pp. 150–159). New York, New York, USA: ACM Press. https://doi.org/10.1145/3301275.3302266

C24
:	Ferwerda, B., **Tkalčič, M.,** Predicting Users’ Personality from Instagram Pictures: Using Visual and/or Content Features?, In UMAP ’18: 26th Conference on User Modeling, Adaptation and Personalization, July 8--11, 2018, Singapore, Singapore. ACM. 

C23
:	 **Tkalčič, M.,** & Ferwerda, B. (2018). Eudaimonic Modeling of Moviegoers. In UMAP ’18: 26th Conference on User Modeling, Adaptation and Personalization, July 8--11, 2018, Singapore, Singapore. ACM. https://doi.org/10.1145/3209219.3209249


C22
:	Ferwerda, B., **Tkalčič, M.,** & Schedl, M. (2017). Personality Traits and Music Genres. In Proceedings of the 25th Conference on User Modeling, Adaptation and Personalization - UMAP ’17 (pp. 285–288). New York, New York, USA: ACM Press. https://doi.org/10.1145/3079628.3079693

C21
:	Ferwerda, B., Graus, M. P., Vall, A., **Tkalčič, M.,** & Schedl, M. (2017). How item discovery enabled by diversity leads to increased recommendation list attractiveness. In Proceedings of the Symposium on Applied Computing - SAC ’17 (pp. 1693–1696). New York, New York, USA: ACM Press. https://doi.org/10.1145/3019612.3019899

C20
:	Ferwerda, B., Schedl, M., & **Tkalčič, M.** (2016). Personality Traits and the Relationship with ( Non- ) Disclosure Behavior on Facebook. WWW’16 Companion. https://doi.org/10.1145/2872518.2890085

C19
:	Ferwerda, B., Schedl, M., & **Tkalčič, M.** (2016). Using Instagram Picture Features to Predict Users’ Personality. In Q. Tian, N. Sebe, G.-J. Qi, B. Huet, R. Hong, & X. Liu (Eds.), Multimedia Modeling (22nd International Conference, MMM 2016, Miami, FL, USA, January 4-6, 2016, Proceedings, Part I) (Vol. 9516, pp. 850–861). Cham: Springer International Publishing. https://doi.org/10.1007/978-3-319-27671-7_71

C18
:	Schedl, M., Hauger, D., **Tkalčič, M.,** Melenhorst, M., & Liem, C. C. S. (2016). A dataset of multimedia material about classical music: PHENICX-SMM. In 2016 14th International Workshop on Content-Based Multimedia Indexing (CBMI) (pp. 1–4). IEEE. https://doi.org/10.1109/CBMI.2016.7500240

C17
:	Kalloori, S., Ricci, F., & **Tkalčič, M.,** (2016). Pairwise Preferences Based Matrix Factorization and Nearest Neighbor Recommendation Techniques. Proceedings of the 10th ACM Conference on Recommender Systems - RecSys ’16, 143–146. https://doi.org/10.1145/2959100.2959142

C16
:	Schedl, M., Eghbal-zadeh, H., Gomez, E., & **Tkalčič, M.** (2016). An Analysis of Agreement in Classical Music Perception and its Relationship to Listener Characteristics. In Proceedings of the 17th ISMIR Conference, New York City, USA, August 7-11, 2016 (pp. 578–583).

C15
:	Ferwerda, B., Vall, A., **Tkalčič, M.,** & Schedl, M. (2016). Exploring Music Diversity Needs Across Countries. In Proceedings of the 2016 Conference on User Modeling Adaptation and Personalization - UMAP ’16 (pp. 287–288). New York, New York, USA: ACM Press. https://doi.org/10.1145/2930238.2930262

C14
:	Skowron, M., Ferwerda, B., **Tkalčič, M.,** & Schedl, M. (2016). Fusing Social Media Cues : Personality Prediction from Twitter and Instagram. WWW’16 Companion, 2–3. https://doi.org/10.1145/2872518.2889368

C13
:	Motajcsek, T., Dobrajs, K., Garzotto, F., Göker, A., Hopfgartner, F., Malagoli, D., **Tkalčič, M.,** ... Demetriou, A. (2016). Algorithms Aside. In Proceedings of the 10th ACM Conference on Recommender Systems - RecSys ’16 (pp. 215–219). New York, New York, USA: ACM Press. https://doi.org/10.1145/2959100.2959164

C12
:	Schedl, M., Melenhorst, M., Liem, C. C. S., Martorell, A., Mayor, Ó., **Tkalčič, M.,** (2016). A Personality-based Adaptive System for Visualizing Classical Music Performances. In Proceedings of the 7th International Conference on Multimedia Systems - MMSys ’16 (pp. 1–7). New York, New York, USA: ACM Press. https://doi.org/10.1145/2910017.2910604

C11
:	**Tkalčič, M.,** Ferwerda, B., Hauger, D., & Schedl, M. (2015). Personality Correlates for Digital Concert Program Notes. In UMAP 2015, Lecture Notes On Computer Science 9146 (Vol. 9146, pp. 364–369). https://doi.org/10.1007/978-3-319-20267-9_32

C10
:	Ferwerda, B., Yang, E., Schedl, M., & **Tkalčič, M.** (2015). Personality Traits Predict Music Taxonomy Preferences. In Proceedings of the 33rd Annual ACM Conference Extended Abstracts on Human Factors in Computing Systems - CHI EA ’15 (pp. 2241–2246). https://doi.org/10.1145/2702613.2732754 

C9
:	Schedl, M., Hauger, D., Farrahi, K., & **Tkalčič, M.** (2015). On the Influence of User Characteristics on Music Recommendation Algorithms. In A. Hanbury, G. Kazai, A. Rauber, & N. Fuhr (Eds.), ECIR 2016, Advances in Information Retrieval Lecture Notes in Computer Science (Vol. 9022, pp. 339–345). Springer. https://doi.org/10.1007/978-3-319-16354-3_37

C8
:	Ferwerda, B., Schedl, M., & **Tkalčič, M.** (2015). Personality & Emotional States : Understanding Users ’ Music Listening Needs. In A. Cristea, J. Masthoff, A. Said, & N. Tintarev (Eds.), UMAP 2015 Extended Proceedings. Retrieved from http://ceur-ws.org/Vol-1388/

C7
:	Farrahi, K., Schedl, M., Vall, A., Hauger, D., & **Tkalčič, M.** (2014). Impact of Listening Behavior on Music Recommendation. In ISMIR 2014. Retrieved from http://www.cp.jku.at/people/schedl/Research/Publications/pdf/farrahi_ismir_2014.pdf

C6
:	Ferwerda, B., Schedl, M., & **Tkalčič, M.** (2014). To Post or Not to Post : The Effects of Persuasive Cues and Group Targeting Mechanisms on Posting Behavior. In 2014 ASE BIGDATA/SOCIALCOM/CYBERSECURITY Conference, Stanford University, May 27-31, 2014. Retrieved from http://www.cp.jku.at/research/papers/Ferwerda_etal_SocialCom_2014.pdf

C5
:	Elahi, M., Braunhofer, M., Ricci, F., & **Tkalčič, M.** (2013). Personality-based active learning for collaborative filtering recommender systems. In M. Baldoni, C. Baroglio, G. Boella, & O. Micalizio (Eds.), AIxIA 2013: Advances in Artificial Intelligence (pp. 360–371). https://doi.org/10.1007/978-3-319-03524-6_31

C4
:	Hauger, D., Schedl, M., Košir, A., & **Tkalčič, M.** (2013). The Million Musical Tweet Dataset: What We Can Learn From Microblogs. In ISMIR 2013. Retrieved from http://www.cp.jku.at/people/schedl/Research/Publications/pdf/hauger_ismir_2013.pdf

C3
:	**Tkalčič, M.,** Burnik, U., Odić, A., Košir, A., & Tasič, J. F. (2013). Emotion-Aware Recommender Systems—A Framework and a Case Study. In S. Markovski & M. Gusev (Eds.), ICT Innovations 2012 Advances in Intelligent Systems and Computing (Vol. 207, pp. 141–150). Berlin, Heidelberg: Springer Berlin Heidelberg. https://doi.org/10.1007/978-3-642-37169-1_14

C2
:	**Tkalčič, M.,** Odić, A., Košir, A., & Tasič, J. (2012). Exploiting implicit affective labeling for image recommendations. In J. Wang, J. del R. Millán, & S. Cho (Eds.), Conference Proceedings - IEEE International Conference on Systems, Man and Cybernetics (pp. 3321–3326). https://doi.org/10.1109/ICSMC.2012.6378304

C1
:	**Tkalčič, M.,** & Tasic, J. F. (2003). Colour spaces: perceptual, historical and applicational background. In B. Zajc & M. Tkalčič (Eds.), The IEEE Region 8 EUROCON 2003. Computer as a Tool. (Vol. 1, pp. 304–308). Proceedings of the IEEE Region 8 EUROCON 2003. Computer as a Tool. https://doi.org/10.1109/EURCON.2003.1248032





### Demos 

D2
:	**Tkalčič, M.,** Maleki, N., Pesek, M., Elahi, M., Ricci, F., & Pesek, M. (n.d.). A Research Tool for User Preferences Elicitation with Facial Expressions. In ACM RecSys 2017 Demo (pp. 1–2). https://doi.org/10.1145/3109859.3109978

D1
:	**Tkalčič, M.,** Schedl, M., Liem, C. C. S. S., & Melenhorst, M. S. (2016). Personalized Retrieval and Browsing of Classical Music and Supporting Multimedia Material. In Proceedings of the 2016 ACM on International Conference on Multimedia Retrieval - ICMR ’16 (pp. 393–396). New York, New York, USA: ACM Press. https://doi.org/10.1145/2911996.2912023




### Workshop Papers 

W23
:	Barile, F., Ricci, F., **Tkalcic, M.,** Magnini, B., Zanoli, R., Lavelli, A., & Speranza, M. (2019). Media Monitoring using News Recommenders. IIR 2019, September 16–18, 2019, Padova, Italy.

W22
:	Moghaddam, F. B., Elahi, M., Hosseini, R., Trattner, C., & **Tkalcic, M.** (2019). Predicting Movie Popularity and Ratings with Visual Features. 2019 14th International Workshop on Semantic and Social Media Adaptation and Personalization (SMAP), 1–6. https://doi.org/10.1109/SMAP.2019.8864912

W21
:	**Tkalcic, M.,** & Ferwerda, B. (2018). Theory-driven Recommendations : Modeling Hedonic and Eudaimonic Movie Preferences. In N. Tonellotto, L. Becchetti, & M. Tkalcic (Eds.), Proceedings of the 9th Italian Information Retrieval Workshop (pp. 1–5).

W20
:	Ferwerda, B., & **Tkalčič, M.** (2018). You Are What You Post : What the Content of Instagram Pictures Tells About Users ’ Personality. In Joint Proceedings of the ACM IUI 2018 Workshops.

W19
:	Ferwerda, B., **Tkalčič**, M., & Schedl, M. (2017). Personality Traits and Music Genre Preferences: How Music Taste Vary Over Age Groups. In RecTemp Workshop in conjunction with Recsys 2017, Como, Italy

W18
:	Ferwerda, B., Graus, M., & Schedl, M. & **Tkalčič, M.** (2016). The Influence of Users ’ Personality Traits on Satisfaction and Attractiveness of Diversified Recommendation Lists. In M. Tkalčič, B. De Carolis, M. de Gemmis, & A. Košir (Eds.), Proceedings of the 4th Workshop on Emotions and Personality in Personalized Systems co-located with ACM Conference on Recommender Systems (RecSys 2016). Boston, MA. Retrieved from http://ceur-ws.org/Vol-1680/

W17
:	Knees, P., Andersen, K., & **Tkalčič, M.** (2015). “I’d like it to do the opposite ”: Music-Making Between Recommendation and Obstruction. In M. Ge & F. Ricci (Eds.), Proceedings of the 2nd International Workshop on Decision Making and Recommender Systems (pp. 1–7).

W16
:	Ferwerda, B., Schedl, M., & **Tkalčič, M.** (2015). Predicting Personality Traits with Instagram Pictures. In M. Tkalčič, B. De Carolis, M. de Gemmis, A. Odić, & A. Košir (Eds.), Proceedings of the 3rd Workshop on Emotions and Personality in Personalized Systems 2015 - EMPIRE ’15 (pp. 7–10). New York, New York, USA: ACM Press. https://doi.org/10.1145/2809643.2809644

W15
:	Schedl, M., & **Tkalčič, M.** (2014). Genre-based Analysis of Social Media Data on Music Listening Behavior. In R. Zimmerman & Y. Yu (Eds.), Proceedings of the First International Workshop on Internet-Scale Multimedia Management - WISMM ’14 (pp. 9–13). New York, New York, USA: ACM Press. https://doi.org/10.1145/2661714.2661717

W14
:	**Tkalčič, M.,** de Gemmis, M., & Semeraro, G. (2014). Personality and Emotions in Decision Making and Recommender Systems. In M. Ge & F. Ricci (Eds.), Proceedings of the First International Workshop on Decision Making and Recommender Systems (DMRS2014) Bolzano, Italy, September 18-19, 2014. Retrieved from http://ceur-ws.org/Vol-1278/paper3.pdf

W13
:	Košir, A., Odić, A., **Tkalčič, M.,** & Svetina, M. (2014). Human decisions in user modeling : motivation , procedure and example application. In I. Cantador, M. Chi, R. Farzan, & R. Jäschke (Eds.), UMAP 2014 Extended Proceedings. Retrieved from http://ceur-ws.org/Vol-1181/empire2014_paper_03.pdf

W12
:	**Tkalčič, M.,** Ferwerda, B., Schedl, M., Liem, C., Melenhorst, M., Odić, A., & Košir, A. (2014). Using social media mining for estimating theory of planned behaviour parameters. In I. Cantador, M. Chi, R. Farzan, & R. Jäschke (Eds.), UMAP 2014 Extended Proceedings (Vol. 1181). Retrieved from http://ceur-ws.org/Vol-1181/empire2014_paper_06.pdf

W11
:	Vodlan, T., **Tkalčič, M.,** & Kosir, A. (2013). The Role of Social Signals in Telecommunication : Experimental Design. In S. Berkovsky, E. Herder, P. Lops, & O. C. Santos (Eds.), UMAP 2013 Extended Proceedings. Retrieved from http://ceur-ws.org/Vol-997/empire2013_paper_6.pdf

W10
:	Košir, A., Odić, A., & **Tkalčič, M.** (2013). How to improve the statistical power of the 10-fold cross validation scheme in recommender systems. In A. Bellogín, P. Castells, A. Said, & D. Tikk (Eds.), Proceedings of the International Workshop on Reproducibility and Replication in Recommender Systems Evaluation - RepSys ’13 (pp. 3–6). New York, New York, USA: ACM Press. https://doi.org/10.1145/2532508.2532510

W9
:	Odić, A., **Tkalčič, M.,** & Košir, A. (2013). Managing Irrelevant Contextual Categories in a Movie Recommender System. In L. Chen, M. de Gemmis, A. Felfernig, P. Lops, F. Ricci, G. Semeraro, & M. Willemsen (Eds.), RecSys’13 Workshop on Human Decision Making in Recommender Systems, 2013, Hong Kong. Retrieved from http://ceur-ws.org/Vol-1050/paper5.pdf

W8
:	Odić, A., **Tkalčič, M.,** Tasič, J. F., & Košir, A. (2013). Personality and Social Context : Impact on Emotion Induction from Movies. In S. Berkovsky, E. Herder, P. Lops, & O. C. Santos (Eds.), UMAP 2013 Extended Proceedings. Retrieved from http://ceur-ws.org/Vol-997/empire2013_paper_5.pdf

W7
:	Odić, A., **Tkalčič, M.,** Tasič, J. F., & Košir, A. (2012). Relevant Context in a Movie Recommender System : Users ’ Opinion vs . Statistical Detection. In G. Adomavicius, L. Baltrunas, E. W. de Luca, T. Hussein, & A. Tuzhilin (Eds.), Proceedings of the 4th Workshop on Context-Aware Recommender Systems in conjunction with the 6th ACM Conference on Recommender Systems (RecSys 2012). Retrieved from http://ceur-ws.org/Vol-889/paper2.pdf

W6
:	**Tkalčič, M.,** Kunaver, M., Košir, A., & Tasič, J. (2011). Addressing the new user problem with a personality based user similarity measure. In F. Ricci, G. Semeraro, M. de Gemmis, P. Lops, J. Masthoff, F. Grasso, & J. Ham (Eds.), Joint Proceedings of the Workshop on Decision Making and Recommendation Acceptance Issues in Recommender Systems (DEMRA 2011) and the 2nd Workshop on User Models for Motivational Systems: The affective and the rational routes to persuasion (UMMS 2011). Retrieved from http://ceur-ws.org/Vol-740/DEMRA_UMMS_2011_proceedings.pdf#page=106

W5
:	**Tkalčič, M.,** Odić, A., Košir, A., & Tasič, J. (2011). Impact of Implicit and Explicit Affective Labeling on a Recommender System’s Performance. Joint Proceedings of the Workshop on Decision Making and Recommendation Acceptance Issues in Recommender Systems (DEMRA 2011) and the 2nd Workshop on User Models for Motivational Systems: The Affective and the Rational Routes to Persuasion (UMMS 2011), 112. Retrieved from http://ceur-ws.org/Vol-740/UMMS2011_paper7.pdf

W4
:	**Tkalčič, M.,** Košir, A., Tasič, J., & Kunaver, M. (2011). Affective recommender systems: the role of emotions in recommender systems. In A. Felfernig, L. Chen, M. Mandl, M. Willemsen, D. Bollen, & M. Ekstrand (Eds.), Joint proceedings of the RecSys 2011 Workshop on Human Decision Making in Recommender Systems (Decisions@RecSys’11) and User-Centric Evaluation of Recommender Systems and Their Interfaces-2 (UCERSTI 2) affiliated with the 5th ACM Conference on Recommender (pp. 9–13). Retrieved from http://ceur-ws.org/Vol-811/paper2.pdf

W3
:	**Tkalčič, M.,** Tasič, J., & Košir, A. (2009). The LDOS-PerAff-1 Corpus of Face Video Clips with Affective and Personality Metadata. In M. Kipp, J.-C. Martin, P. Paggio, & D. Heylen (Eds.), Proceedings of Multimodal Corpora: Advances in Capturing, Coding and Analyzing Multimodality (Malta, 2010), LREC (p. 111). Retrieved from http://embots.dfki.de/doc/MMC2010-Proceedings.pdf

W2
:	**Tkalčič, M.,** Kunaver, M., Tasič, J., & Košir, A. (2009). Personality Based User Similarity Measure for a Collaborative Recommender System. In C. Peter, E. Crane, L. Axelrod, H. Agius, S. Afzal, & M. Balaam (Eds.), 5th Workshop on Emotion in Human-Computer Interaction-Real World Challenges (p. 30). Retrieved from http://publica.fraunhofer.de/documents/N-113443.html

W1
:	**Tkalčič, M.,** Tasič, J. F., & Košir, A. (2009). Emotive and Personality Parameters in Multimedia Recommender Systems. In A. Vinciarelli, C. Pelachaud, R. Cowie, & A. Nijholt (Eds.), Affective Computing and Intelligent Interaction Proceedings of the Doctoral Consortium 2009 (1st ed., p. 33). CTIT Workshop Proceedings Series WP09-13. Retrieved from http://www.utwente.nl/ctit/library/proceedings/wp0913.pdf






### Books, Edited Volumes and Book Chapters

B18
:	Masthoff, J., Herder, E., Tintarev, N., & **Tkalčič, M.** (2021). UMAP ’21: Proceedings of the 29th ACM Conference on User Modeling, Adaptation and Personalization. Association for Computing Machinery.

B17
:	Masthoff, J., Herder, E., Tintarev, N., & **Tkalčič, M.** (2021). UMAP ’21: Adjunct Proceedings of the 29th ACM Conference on User Modeling, Adaptation and Personalization. Association for Computing Machinery.

B16
:	Delić, A., Nguyen, T. N., & **Tkalčič, M.** (2020). Group Decision-Making and Designing Group Recommender Systems. In Handbook of e-Tourism (pp. 1–23). Springer International Publishing. https://doi.org/10.1007/978-3-030-05324-6_57-1


B15
:	**Tkalcic, M.**, Pera S., Proceedings of ACM RecSys 2019 Late-breaking Results (ACM RecSys LBR 2019), Copenhagen, Denmark, September 16-20, 2019. http://ceur-ws.org/Vol-2431/

B14
:	Tonellotto, N., Becchetti, L., **Tkalčič, M.**, Proceedings of the 9th Italian Information Retrieval Workshop (IIR 2018), Rome, Italy, May, 28-30, 2018. http://ceur-ws.org/Vol-2140/

B13 
:	Felfernig, A., Boratto, L., Stettinger, M., & **Tkalčič, M.** (2018). Group Recommender Systems. Springer International Publishing. https://doi.org/10.1007/978-3-319-75067-5

B12 
:	**Tkalčič, M.**, Delić, A., & Felfernig, A. (2018). Personality, Emotions, and Group Dynamics. In A. Felfernig, L. Boratto, Martin Stettinger, & M. Tkalčič (Eds.), Group Recommender Systems An Introduction (pp. 157–167). https://doi.org/10.1007/978-3-319-75067-5_9

B11
: **Tkalčič, M.** (2017). Emotions and Personality in Recommender Systems. In Encyclopedia of Social Network Analysis and Mining (2nd ed., pp. 1–9). Springer New York. https://doi.org/10.1007/978-1-4614-7163-9_110161-1

B10
:	**Tkalčič, M.,** Thakker, D., Germanakos, P., Yacef, K., Paris, C., & Santos, O. (Eds.). (2017). Adjunct Publication of the 25th Conference on User Modeling, Adaptation and Personalization. ACM New York, NY, USA. Retrieved from http://dl.acm.org/citation.cfm?id=3099023

B9
:	Odić, A., Košir, A., & **Tkalčič**, M. (2016). Affective and Personality Corpora. In M. Tkalčič, B. De Carolis, M. de Gemmis, A. Odic, & A. Košir (Eds.), Emotions and Personality in Personalized Services (pp. 163–178). Springer. https://doi.org/10.1007/978-3-319-31413-6_9

B8
:	**Tkalčič, M.,** De Carolis, B., de Gemmis, M., Odić, A., & Košir, A. (2016). Introduction to Emotions and Personality in Personalized Systems. In M. Tkalčič, B. De Carolis, M. de Gemmis, A. Odic, & A. Košir (Eds.), Emotions and Personality in Personalized Services (pp. 3–11). Springer. https://doi.org/10.1007/978-3-319-31413-6_1

B7
:	**Tkalčič, M.,** Carolis, B. De, Gemmis, M. de, Odić, A., & Košir, A. (Eds.). (2016). Emotions and Personality in Personalized Services. Springer International Publishing. https://doi.org/10.1007/978-3-319-31413-6

B6
:	**Tkalčič, M.,** De Carolis, B., de Gemmis, M., Odić, A., & Košir, A. (2016). Proceedings of the 4th Workshop on Emotions and Personality in Personalized Systems (EMPIRE 2016), Boston, MA, USA, September 16, 2016. http://ceur-ws.org/Vol-1680/

B5
:	**Tkalčič, M.,** & Chen, L. (2015). Personality and Recommender Systems. In F. Ricci, L. Rokach, & B. Shapira (Eds.), Recommender Systems Handbook (2nd ed., Vol. 54, pp. 715–739). Boston, MA: Springer US. https://doi.org/10.1007/978-1-4899-7637-6_21

B4
:	**Tkalčič, M.,** De Carolis, B., de Gemmis, M., Odić, A., & Košir, A. (2015). Proceedings of the 3rd Workshop on Emotions and Personality in Personalized Systems 2015, http://dl.acm.org/citation.cfm?id=2809643&preflayout=flat#source

B3
:	**Tkalčič, M.,** Tasič, J. F., & Košir, A. (2012). The Need for Affective Metadata in Content-Based Recommender Systems for Images. In M. Maybury (Ed.), Multimedia Information Extraction: Advances in Video, Audio, and Imagery Analysis for Search, Data Mining, Surveillance, and Authoring. Wiley - IEEE Computer Society Press. https://doi.org/10.1002/9781118219546.ch19

B2
:	**Tkalčič**, M., Košir, A., & Tasič, J. F. (2011). Emotive and personality parameters in recommender systems: Recognition and usage of user-centric data for user and item modeling in content retrieval systems. LAP LAMBERT Academic Publishing. 

B1
:	**Tkalčič, M.,** & Pogačnik, M. (2006). Tourist Adapted Destination Selection. In R. Ovsenik & I. Kiereta (Eds.), Destination Management (pp. 195–209). Peter Lang. Retrieved from http://www.peterlang.de/index.cfm?event=cmp.ccc.seitenstruktur.detailseiten&seitentyp=produkt&pk=39292&CFID=200073&CFTOKEN=50206088


### Other

O4
:	Ferwerda, B., Chen, L., & **Tkalčič, M.** (2021). Editorial: Psychological Models for Personalized Human-Computer Interaction (HCI). Frontiers in Psychology, 12, 673092. https://doi.org/10.3389/fpsyg.2021.673092


O3
:	**Tkalčič, M.**, Schedl, M., & Knees, P. (2020). Preface to the Special Issue on user modeling for personalized interaction with music. User Modeling and User-Adapted Interaction, 0123456789, 1–4. https://doi.org/10.1007/s11257-020-09264-6

O2
:	**Tkalčič, M.,** Quercia, D., & Graf, S. (2016). Preface to the special issue on personality in personalized systems. User Modeling and User-Adapted Interaction, 26(2–3), 103–107. https://doi.org/10.1007/s11257-016-9175-9 (non peer reviewed)

O1
:	Gemmis, M. de, Carolis, N. De, Košir, A., & **Tkalčič, M.** (2016). Emotions and Personality in Personalized Systems. Interaction Design and Architecture(s) Journal - IxD&A, 28, 105–109. (non peer reviewed)

### Invited Talks

2021
:	Invited talk at the **University of Bergen**, Norway within the Media Futures project, *Computational Psychology in Recommender Systems*, 8. June 2021, https://mediafutures.no/event/seminar-computational-psychology-in-recommender-systems-marko-tkalcic-university-of-primorska-slovenia/

2020
:	Keynote talk at the **ISMIS 2020 conference**, *Complementing Behavioural Modeling with Cognitive Modeling for Better Recommendations*. In Lecture Notes in Computer Science (including subseries Lecture Notes in Artificial Intelligence and Lecture Notes in Bioinformatics): Vol. 12117 LNAI (pp. 3–8). https://doi.org/10.1007/978-3-030-59491-6_1

2020
:	Invited talk at **Durham University**, UK, *Emotions and Personality for Better Recommendations*, 9. March 2020

2020
:	Invited talk at the **Graz University of Technology**, Austria, 27. February 2020

2019
:	Keynote talk at the **AI Journey** conference in Moscow, Russia, *From Behavioural to Cognitive Modeling in Recommender Systems*, https://ai-journey.ru/en/conference-moscow/science-day-program

2019
:	Invited talk at the **Robert Bosch GmbH Center for Research and Development**, Stuttgart, Germany 19. February 2019

2017
:	Invited talk at the **Alpen-Adria-Universität Klagenfurt**, Austria, *Affective Personalization - from Psychology to Algorithms*, 21. December 2017, https://www.ftf.or.at/2017/12/affective-personalization-from-psychology-to-algorithms/

2017
:	Invited talk at the **Jonkoping University**, Sweden, *Bridging computer-science and psychological models for personalization* 5. October 2017

2017
:	Invited talk at the **Vienna University of Technology**, Austria *Psychologically-driven Personalization*, 10. April 2017

2016
:	Invited talk at the **University in Ljubljana**, Slovenia Faculty of Computer Science: *Psychologically-driven Personalization*, https://www.fri.uni-lj.si/en/news/article/fri-piskot-seminar-Tkalčič-psychologically-driven-personalization, 22. December 2016

2016
:	Invited talk at the **Johannes Kepler University**, Linz, Austria: *Learning from User-generated Data*, 21. June 2016

2015
:	Invited talk at the **Brain Week 2015** conference, Ljubljana: *Tell me what you like and I tell you who you are: social media, personality and emotions*, 18. March 2015, http://www.sinapsa.org/tm/program/2015-03-18/Ljubljana 

2015
:	Invited talk at the **Graz University of Technology**, Austria, *Affect- and Personality-based Recommendations*, 26. January 2015, http://ase.ist.tugraz.at/ASE/?page_id=224 

2014
:	Invited talk at the **International Workshop on Decision Making and Recommender Systems 2014**, Bolzano, Italy: *Decision Making, Personality and Emotions* (with Giovanni Semeraro and Marco de Gemmis), 18. September 2014, http://dmrsworkshop.inf.unibz.it/2014/ 

2013
:	Invited talk at the **Johannes Kepler University** Linz, Austria, *Emotions, personality and recommender systems*, Department of Computational Perception, 12. February, 2013

2012
:	Invited talk at the **Conference of Cognitive Sciences – Information Society 2012**: *Automatic Detection of Emotions*, 8-12. October, 2012, Jožef Stefan Institute, Ljubljana, Slovenia http://is.ijs.si/is/is2012/konference/Kognitivna/KZ-IS-2012-ENG.pdf 

2012
:	Invited talk at the **Free University of Bolzano**, Italy: *Affect in recommender systems*. Bolzano: Libera Università, 4. September 2012 https://www.inf.unibz.it/dis/wp/?page_id=100#Tkalčič 

### Tutorials and Summer Schools

2018
:	ACM RecSys 2018, Vancouver: Tutorial *Emotions and Personality in Recommender Systems*

2017
:	ACM Summer School on Recommender Systems, Bozen-Bolzano, 21-25 August 2017 *Affect and Personality-Based RS*








<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-102616018-1', 'auto');
  ga('send', 'pageview');
</script>